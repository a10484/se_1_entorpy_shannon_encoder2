from itertools import count
from multiprocessing.sharedctypes import Value
import string
from collections import Counter
import matplotlib.pyplot as plt
import math
from texttable import Texttable
import float2binary as f2b


text = """The Amazon Rainforest is the largest tropical jungle in the world, containing some of the most biodiverse plants species in the world. Located in South America, more than 60,000 species of plants grow in the Amazon jungle. 20% of the world's natural forest terrain is the Amazon Jungle itself. Scientists regularly discover new and important plant species, many of them hold medicinal properties. Below we have listed some of the most interesting plants found in the Amazon. Giant water lilies are one of the more memorable plants you will find in the Amazon Rainforest. Named after Queen Victoria, the sheer size of the Victoria Amazonica water lilies is what sets them apart. They can grow up to 10 feet (or 3 meters) in diameter and can hold up to 60 pounds of weight. Victoria Amazonica lilies grow in the Pacaya Samiria National Reserve in Peru."""
# text = "this is a test"

# a -
# 1 - تمامي علائم نقطه گذاري به همراه خط فاصله حذف شود.

# define punctuation
# punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
punctuations = string.punctuation
punctuations_with_numbers = punctuations + "0123456789"


text_without_whitespace = text.replace(" ", "")

# remove punctuation from the text
text_without_punctuation = ""
for char in text_without_whitespace:
    if char not in punctuations_with_numbers:
        text_without_punctuation = text_without_punctuation + char.lower()

# display the unpunctuated string
# print(text_without_punctuation)

# print("=================================================================================================1")

list_of_character = []
list_of_character[:0] = text_without_punctuation
# list_of_character.sort()
count_of_list_of_characters = len(list_of_character)

set_list_of_character = set(list_of_character)

# print(list_of_character, " ===>>> ", len(list_of_character))
print("=================================================================================================")
print(set_list_of_character, " ===>>> ", len(set_list_of_character))
print("=================================================================================================")


# count_repeated_elements = {i:list_of_character.count(i) for i in list_of_character}
count_repeated_elements = dict(Counter(list_of_character))


# count_repeated_elements = dict(sorted(count_repeated_elements.items()))
sorted_count_repeated_elements = {k: v for k, v in sorted(
    count_repeated_elements.items(), key=lambda item: item[1], reverse=True)}

# print(count_repeated_elements, " 222===>>> ", len(count_repeated_elements))
# print("=================================================================================================4")
probabilities_elements = {
    key: sorted_count_repeated_elements[key] / count_of_list_of_characters for key in sorted_count_repeated_elements.keys()}


# https://en.wikipedia.org/wiki/Shannon_coding
# probabilities_elements = {'A': 0.36, 'B': 0.18,
#                           'C': 0.18, 'D': 0.12, 'E': 0.09, 'F': 0.07}


print(probabilities_elements)
print("=================================================================================================")


names = list(probabilities_elements.keys())
values = list(probabilities_elements.values())

plt.bar(range(len(probabilities_elements)), values, tick_label=names)
plt.show()

H_entropy = 0

# code_word_lengths = sorted_count_repeated_elements.copy()
cumulative_probabilities = sorted_count_repeated_elements.copy()
cumulative_probabilitie = 0

for key in probabilities_elements:
    information = round(-math.log(probabilities_elements[key], 2), 4)
    # code_word_lengths[key] = math.ceil(information)
    cumulative_probabilities[key] = cumulative_probabilitie
    cumulative_probabilitie += probabilities_elements[key]
    H_entropy += probabilities_elements[key] * information

print("H_entropy ==> ", H_entropy)
print("=================================================================================================")

H_max = round(math.log(len(probabilities_elements), 2), 4)
print("H_max ==> ", H_max)
print("=================================================================================================")


efficiency = H_entropy / H_max
print("efficiency ==> ", efficiency)
print("=================================================================================================")


redundancy = 1 - efficiency
print("redundancy ==> ", redundancy)
print("=================================================================================================")


# print("code_word_lengths ==> ", code_word_lengths)
# print("=================================================================================================")
# print("cumulative_probabilitie ==> ", cumulative_probabilitie)
# print("=================================================================================================")
# print("cumulative_probabilities ==> ", cumulative_probabilities)
# print("=================================================================================================")


print()
# https://pypi.org/project/texttable/
table = Texttable()
# table.set_deco(Texttable.VLINES)
table.set_cols_width(["3", "7", "13", "13", "5", "15", "6"])
table.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
table.set_cols_valign(["m", "m", "m", "m", "m", "m", "m"])
table.set_cols_dtype(['a', 'a', 'a', 'a', 'a', 't', 'a'])
table_content = [['row', 'Symbols', 'Probabilities',
                  'Information', 'f_i', 'codewords', "n_i"]]

i = 1
n_bar = 0


for key in probabilities_elements:
    information = round(-math.log(probabilities_elements[key], 2), 4)
    n_bar += probabilities_elements[key] * math.ceil(information)
    try:
        row = [i, key, probabilities_elements[key],
               f"{information} bit", cumulative_probabilities[key], f2b.float_bin(float(cumulative_probabilities[key]), places=math.ceil(information))[1:], math.ceil(information)]
    except:
        print("----------------------------")
        print(f"error => {key}")
        print("----------------------------")
        row = [i, key, probabilities_elements[key],
               f"{information} bit", cumulative_probabilities[key], "0" * math.ceil(information), math.ceil(information)]
    table_content.append(row)
    i += 1


# f2b.float_bin(float(cumulative_probabilities[key]), places=math.ceil(information))

table.add_rows(table_content)
print(table.draw())


print()

e_prime = H_entropy/n_bar

info_table = Texttable()
# table.set_deco(Texttable.VLINES)
info_table.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c"])
info_table.set_cols_valign(["m", "m", "m", "m", "m", "m", "m", "m"])
info_table.add_rows([['H', "H'", 'e', "e'", 'p', "p'", 'n_bar', 'H_max'], [
                    H_entropy, e_prime, efficiency, e_prime, redundancy, 1 - e_prime, n_bar, H_max]])
print(info_table.draw())

# print("Symbols | Probabilities | Information | codewords | H | H' | e | e' | p | p' | n_bar")
# for key in probabilities_elements:
#     print(
#         f"{key} | {probabilities_elements[key]} | {-math.log(probabilities_elements[key],2)} | codewords | H | H' | e | e' | p | p' | n_bar")
